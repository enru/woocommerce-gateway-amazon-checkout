<?php
/*
Plugin Name: WooCommerce Gateway for Amazon Checkout
Plugin URI: http://inigo.net/woocommerce-gateway-amazon-checkout
Description: Extends WooCommerce with the Amazon Checkout.
Version: 0.1.11
Author: Neill Russell (@enru)  
Author URI: http://enru.co.uk/

Copyright: © 2012 Inigo Media Ltd.
License: GNU General Public License v2.0
License URI: http://www.gnu.org/old-licenses/gpl-2.0.html
*/

/** 
 * Create the associated DB tables on activation
 **/
//require 'classes/class-amazon-checkout-db.php';
//register_activation_hook(__FILE__, array('WC_Amazon_Checkout_Db', 'create_db'));

/**
 * Check if WooCommerce is active
 **/
if(in_array('woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' )))) {

    add_action('plugins_loaded', 'woocommerce_amazon_checkout_init', 0);

    function woocommerce_amazon_checkout_init() {

        if(class_exists('WC_Amazon_Checkout')) return;
        
        // shortcut to our plugin's base dir
        define("AMAZON_CHECKOUT_DIR", dirname(__FILE__));

        /**
        * Localisation
        */
        load_plugin_textdomain(
            'wc-amazon-checkout', 
            false, 
            dirname(plugin_basename( __FILE__ )) . '/languages');

        /**
        * Add the Gateway to WooCommerce
        **/
        function woocommerce_add_amazon_checkout_gateway($methods) {
            $methods[] = 'WC_Amazon_Checkout';
            return $methods;
        }
        add_filter('woocommerce_payment_gateways', 'woocommerce_add_amazon_checkout_gateway');
        add_filter('woocommerce_available_payment_gateways', 'restrict_amazon_checkout');

        /**
        * Load Amazon Checkout classes
        **/
        require AMAZON_CHECKOUT_DIR.'/classes/class-wc-amazon-checkout.php';
        $amazon_checkout = new WC_Amazon_Checkout();

        // notify if wrong currency
        add_action('admin_notices', array($amazon_checkout, 'check_currency'));
    }

    /**
     * Remove Amazon Checkout from the standard checkout
     **/
    function restrict_amazon_checkout($available_gateways) {
        if(is_checkout() || (is_ajax() && isset($_POST['action']) && $_POST['action'] == 'woocommerce_update_order_review')) {
            foreach($available_gateways as $key => $gateway) {
                if($gateway->id == 'amazon-checkout') {
                    unset($available_gateways[$key]);
                    break;
                }
            }
        }
        return $available_gateways;
    }


    /*
     * place a 'get billing address form amazon' button  
     * under billing address in WC admin
     * some shops have experienced issues with wp_cron
     */
    function amazon_checkout_get_billing_address_button($order) {
        echo '<div class="edit_address"><p><button class="button get_amazon_address">'. __( 'Get Address from Amazon', 'amazon-checkout' ) . '</button></p></div>';
    }
    add_action('woocommerce_admin_order_data_after_shipping_address', 'amazon_checkout_get_billing_address_button', 10, 1);

    /*
     * add our admin JS scripts to admin panel
     */
    function amazon_checkout_admin_enqueue_scripts() {
        wp_register_script('amazon-checkout-admin', plugin_dir_url(__FILE__) . '/assets/js/amazon-checkout-admin.js', array('jquery'), null); //$woocommerce->version );
		wp_enqueue_script('amazon-checkout-admin' );
    }
    add_action('admin_enqueue_scripts', 'amazon_checkout_admin_enqueue_scripts');

    /*

						$('input#_shipping_first_name').val( info.shipping_first_name );
						$('input#_shipping_last_name').val( info.shipping_last_name );
						$('input#_shipping_company').val( info.shipping_company );
						$('input#_shipping_address_1').val( info.shipping_address_1 );
						$('input#_shipping_address_2').val( info.shipping_address_2 );
						$('input#_shipping_city').val( info.shipping_city );
						$('input#_shipping_postcode').val( info.shipping_postcode );
						$('#_shipping_country').val( info.shipping_country );
						$('input#_shipping_state').val( info.shipping_state );
                        */

} // end if woocommerce

