<?php
class WC_Amazon_Checkout_Widgets {

    private $merchant_id = null;

    public function __construct($merchant_id) {
        $this->merchant_id = $merchant_id;

    }

    /**
     * Amazon Checkout button
     * displays amazon checkout button
     */
    function checkout_button() { ?>
<div id="AmazonInlineCheckoutWidget"></div>
<script>
new CBA.Widgets.InlineCheckoutWidget({
    merchantId: "<?php echo esc_js($this->merchant_id); ?>",
    design: { size: { width:'126', height:'24' } },
    onAuthorize: function(widget) {
        window.location = '<?php echo site_url('/amazon/checkout'); ?>/?purchaseContractId='+widget.getPurchaseContractId();
    }
}).render("AmazonInlineCheckoutWidget");
</script>
<?php 
    }

    /**
     * Widget
     */
    function display_widget($widget, $mode='edit', $callback) { 
        $widget_id = sprintf("Amazon%s_%d", $widget, rand()); 
?>
<div id="<?php esc_attr_e($widget_id); ?>"></div>
<script>
new CBA.Widgets.<?php echo esc_js($widget); ?>({
    merchantId: "<?php echo esc_js($this->merchant_id); ?>",
    displayMode: "<?php echo esc_js(ucfirst($mode)); ?>",
    <?php if($mode == 'read'): ?>
    design:{size: { width:'277' } },
    <?php endif; ?>

    <?php echo $callback['name']; ?>: <?php echo esc_js($callback['func']); ?>
}).render("<?php echo esc_js($widget_id); ?>");
</script>
<?php 
    }

    /**
     * Address Widget
     * displays address widget
     */
    function address_widget($mode='edit') { 
        $callback = array(
            'name' => 'onAddressSelect', 
            'func' => 'function(widget) {'
            .'}',
        );
        $this->display_widget('AddressWidget', $mode, $callback);
    /*
        $widget_id = sprintf("AmazonAddressWidget_%d", rand()); 
?>
<div id="<?php esc_attr_e($widget_id); ?>"></div>
<script>
new CBA.Widgets.AddressWidget({
    merchantId: "<?php echo esc_js($this->merchant_id); ?>",
    displayMode: "<?php echo esc_js(ucfirst($mode)); ?>",
    onAddressSelect: function(widget) {  
        // Replace the following code with the action you want to perform 
        // after the address is selected.
        // widget.getPurchaseContractId() returns the
        // PurchaseContractId, which can be used to retrieve the address details
        // by calling CheckoutByAmazon service. 
        //document.getElementById("continue_button").disabled=false; 
        <?php if($mode =='edit'): ?>
            jQuery("#amazon-checkout").accordion( "option", "active", 1);
        <?php endif; ?>
    }
}).render("<?php echo esc_js($widget_id); ?>");
</script>
<?php 
    */
    }

    /**
     * Address Book Widget
     * displays address book for standard checkout
     */
    function addressbook_widget($mode='edit') {
        $widget_id = sprintf("AmazonInlineWidget_%d", rand()); 
?>
<div id="<?php esc_attr_e($widget_id); ?>"></div>
<script type='text/javascript' >
new CBA.Widgets.InlineCheckoutWidget({
    merchantId: "<?php echo esc_js($this->merchant_id); ?>",
    displayMode: "<?php echo esc_js(ucfirst($mode)); ?>",
    // The onAuthorize callback function is invoked after the user is successfully authenticated.
    // The callback function is passed the widget reference and widget.getPurchaseContractId() 
    // returns the PurchaseContractId, which can be used to retrieve the address details
    // by calling CheckoutByAmazon service or completing the order. 
    onAuthorize: function(widget) {
    }
}).render("<?php echo esc_js($widget_id); ?>");
</script>
<?php 
    }

    /**
     * Wallet Widget
     * displays payment options
     */
    function wallet_widget($mode='edit') {
        $widget_id = sprintf("AmazonWalletWidget_%d", rand()); 
?>
<div id="<?php esc_attr_e($widget_id); ?>"></div>
<div id="AmazonWalletWidget"></div>
<script>
new CBA.Widgets.WalletWidget({
    merchantId: "<?php echo esc_js($this->merchant_id); ?>",
    displayMode: "<?php echo esc_js(ucfirst($mode)); ?>",
    <?php if($mode == 'read'): ?>
    design:{size: { width:'320' } },
    <?php endif; ?>
    onPaymentSelect: function(widget) {   
        //document.getElementById("continue_button").disabled=false;  
    }
}).render("<?php echo esc_js($widget_id); ?>");
</script>
<?php
    }

    /**
     * Order Details Widget
     * displays order details
     */
    function orderdetails_widget() {
?>
<p>Your Amazon Payments Order ID is <a href="#" onclick="orderDetailsWidget.open('230px', '554px');">
    <!-- Replace "AMAZON ORDER ID" with the Amazon Order Id -->
    AMAZON ORDER ID
</a>.
</p>
<script>
var orderDetailsWidget = new CBA.Widgets.OrderDetailsWidget({
    merchantId: "<?php echo esc_js($this->merchant_id); ?>",
    // Replace "AMAZON ORDER ID" with the Amazon Order Id
    orderID: 'AMAZON ORDER ID',
    design:{size: { width:'392', height:'306' } }
});
</script>
<?php
    }
}

