<?php
require_once 'class-wc-amazon-checkout-client.php';

class WC_Amazon_Checkout_Client_CBA extends WC_Amazon_Checkout_Client  {

    protected $_version = '2010-08-31';

    function get_address($purchase_contract_id) {

        // make the call
        $response = $this->httpPost(array(
            'Action'=>'GetPurchaseContract',
            'PurchaseContractId'=>$purchase_contract_id,
        ));

        // process the response
        if($response && 
            isset($response['Status']) && 
            isset($response['ResponseBody'])) {

            if(200 == intval($response['Status'])) {
                $xml = new SimpleXMLElement($response['ResponseBody']);
                $xml->registerXPathNamespace("ac", "http://payments.amazon.com/checkout/v2/2010-08-31/");
                if($result = $xml->xpath('//ac:ShippingAddress')) {
                    return $result[0];
                }
            }

        }

        return (object) null;
    }

    function set_purchase_items($purchase_contract_id, $cart) {

        // make the call
        $params = array(
            'Action'=>'SetPurchaseItems',
            'PurchaseContractId'=>$purchase_contract_id,
        );

        $params = array_merge($params, $cart);
        error_log(var_export($params, true));

        $response = $this->httpPost($params);
        
        if(200 != intval($response['Status'])) {
            error_log(var_export($response, true));
            $e = 'Problem sending items to Amazon Checkout';
            $xml = new SimpleXMLElement($response['ResponseBody']);
            $xml->registerXPathNamespace("ac", "http://payments.amazon.com/checkout/v2/2010-08-31/");
            if($result = $xml->xpath('//ac:Error')) {
                $e = (string) $result[0]->Message;
            }
            error_log($e);
            //throw new Exception($e);
        }
    }

    function set_contract_charges($purchase_contract_id, $currency, $shipping, $discount=0) {

        $data = array(
            'Action'=>'SetContractCharges',
            'PurchaseContractId'=>$purchase_contract_id,
            'Charges.Shipping.Amount'=>$shipping,
            'Charges.Shipping.CurrencyCode'=>$currency,
        );

        if($discount > 0) {
            $data['Charges.Promotions.Promotion.1.PromotionId']='Discount';
            $data['Charges.Promotions.Promotion.1.Description']='Discount on order';
            $data['Charges.Promotions.Promotion.1.Discount.Amount']=$discount;
            $data['Charges.Promotions.Promotion.1.Discount.CurrencyCode']=$currency;
        }

        // make the call
        $response = $this->httpPost($data);
        
        if(200 != intval($response['Status'])) {
            error_log(var_export($response, true));
            $e = 'Problem sending items to Amazon Checkout';
            $xml = new SimpleXMLElement($response['ResponseBody']);
            $xml->registerXPathNamespace("ac", "http://payments.amazon.com/checkout/v2/2010-08-31/");
            if($result = $xml->xpath('//ac:Error')) {
                $e = (string) $result[0]->Message;
            }
            error_log($e);
            //throw new Exception($e);
        }

    }

    function complete_purchase_contract($purchase_contract_id, $integrator_id='Inigo') {

        // make the call
        $response = $this->httpPost(array(
            'Action'=>'CompletePurchaseContract',
            'PurchaseContractId'=>$purchase_contract_id,
            'IntegratorId'=>$integrator_id,
            'IntegratorName'=>get_bloginfo('name'),
            //'InstantOrderProcessingNotificationURLs.IntegratorURL'=>home_url(),
            //'InstantOrderProcessingNotificationURLs.MerchantURL'=>home_url(),
        ));
        
        // process the response
        if($response && 
            isset($response['Status']) && 
            isset($response['ResponseBody'])) {

            $xml = new SimpleXMLElement($response['ResponseBody']);
            $xml->registerXPathNamespace("ac", "http://payments.amazon.com/checkout/v2/2010-08-31/");

            if(200 == intval($response['Status'])) {
                if($result = $xml->xpath('//ac:CompletePurchaseContractResult/ac:OrderIds/ac:OrderId')) {
                    return (string) $result[0];
                }
            }
            else {
                $msg = "Error processing Amazon payment";
                if($result = $xml->xpath('//ac:Error/ac:Message')) {
                    $msg = (string) $result[0];
                }
                error_log(var_export($response, true));
                //throw new Exception($msg);
            }

        }
        return null;

    }

}

