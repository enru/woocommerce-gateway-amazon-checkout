<?php
require_once 'class-wc-amazon-checkout-client.php';

class WC_Amazon_Checkout_Client_MWS extends WC_Amazon_Checkout_Client  {

    private $_document_version = '1.01';

    function get_order($order_id, $merchant_id) {

        // set Order API Version
        $this->_version = '2011-01-01';

        $endpoint_uri = 'Orders/'.$this->_version;

        // make the call
        $response = $this->httpPost(array(
            'Action'=>'GetOrder',
            'SellerId'=>$merchant_id,
            'AmazonOrderId.Id.1'=>$order_id,
        ), $endpoint_uri);

        // process the response
        if($response && 
            isset($response['Status']) && 
            isset($response['ResponseBody'])) {

            $xml = new SimpleXMLElement($response['ResponseBody']);
            $xml->registerXPathNamespace("ac", "https://mws.amazonservices.com/".$endpoint_uri);

            if(200 == intval($response['Status'])) {

                if($result = $xml->xpath('//ac:GetOrderResult/ac:Orders/ac:Order/ac:ShippingAddress')) {
                    error_log(var_export($result, true));
                    return (array) $result[0];
                }
            }
            else {
                $msg = "Error processing Amazon order";
                if($result = $xml->xpath('//ac:Error/ac:Message')) {
                    $msg = (string) $result[0];
                }
                error_log(var_export($response, true));
                //throw new Exception($msg);
            }

        }
        return null;

    }

    private function _create_envelope($message_type) {
    
        // create the XML envelope & fill with data
        $envelope = new SimpleXMLElement("<AmazonEnvelope></AmazonEnvelope>");
        $envelope->addAttribute('xmlns:xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
        $envelope->addAttribute('xsi:xsi:noNamespaceSchemaLocation','amzn-envelope.xsd');

        $envelope->Header->DocumentVersion = $this->_document_version;

        if(function_exists('get_bloginfo')) {
            $envelope->Header->MerchantIdentifier = get_bloginfo('name');
        }

        // Assigning the MessageType
        $envelope->MessageType = $message_type;

        return $envelope;

    }

    private function _submit_feed($feed, $feed_type, $merchant_id) {

        // set FEED API Version
        $this->_version = '2009-01-01';

        $parameters = array(
            'Action'=>'SubmitFeed',
            'Merchant'=>$merchant_id,
            'AWSAccessKeyId'=>$this->_awsAccessKeyId,
            'FeedType'=>$feed_type,
        );

        $response = $this->httpPost($parameters, '', $feed);

        error_log(var_export($response, true));
    }

    /** 
     * cancels an order
     * wrapper for ack_order()
     */
    function cancel_order($order_id, $wc_order_id=null, $merchant_id, $status='Success') {
        $this->ack_order($order_id, $wc_order_id, $merchant_id, 'Failure');
    }

    /**
     * acknowledge an order
     * to confirm send "Success" as status
     * to cancel send "Failure" as status
     */
    function ack_order($order_id, $wc_order_id=null, $merchant_id, $status='Success') {
    
        // create the XML envelope & fill with data
        $envelope = $this->_create_envelope($message_type="OrderAcknowledgement");

        // <Message> block
        $envelope->Message[0]->MessageID = 1;
        $envelope->Message[0]->OrderAcknowledgement->AmazonOrderID = $order_id;
        if(!is_null($wc_order_id)) {
            $envelope->Message[0]->OrderAcknowledgement->MerchantOrderID = $wc_order_id;
        }

        // set msg data
        $envelope->Message[0]->OrderAcknowledgement->StatusCode = $status;

        $this->_submit_feed($envelope->asXML(), '_POST_ORDER_ACKNOWLEDGEMENT_DATA_', $merchant_id);

    }

    function confirm_shipment($wc_order_id, $merchant_id) {

        $wc_order = new WC_Order($wc_order_id);
        //$this->_document_version = '1.02';

        // only continue if WC order has an Amazon Order ID
        if($amazon_checkout_order_id = get_post_meta($wc_order_id, 'amazon_checkout_order_id', true)) {

            // only confirm processing orders
            //if($wc_order->status == 'processing') {

                // create the XML envelope & fill with data
                $envelope = $this->_create_envelope($message_type="OrderFulfillment");

                // <Message> block
                $envelope->Message[0]->MessageID = 1;
                $envelope->Message[0]->OrderFulfillment->AmazonOrderID = $amazon_checkout_order_id;
                //if(!is_null($wc_order_id)) {
                    //$envelope->Message[0]->OrderFulfillment->MerchantOrderID= $wc_order_id;
                //}

                // set msg data
                $envelope->Message[0] ->OrderFulfillment->FulfillmentDate = date('Y-m-d\TH:i:sP');
                $envelope->Message[0] ->OrderFulfillment->FulfillmentData->CarrierName= "ACME Carriers LTD";
                $envelope->Message[0] ->OrderFulfillment->FulfillmentData->ShippingMethod = $wc_order->get_shipping_method();
                //$envelope->Message[0] ->OrderFulfillment->FulfillmentData->ShipperTrackingNumber = "122-222-22-2-2";

                error_log(var_export($envelope->asXML(), true));

                $this->_submit_feed($envelope->asXML(), '_POST_ORDER_FULFILLMENT_DATA_', $merchant_id);
            //}
        }
    }

}

