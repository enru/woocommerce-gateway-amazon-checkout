<?php
/**
* Helper class
*/
class WC_Amazon_Checkout_Helper {

  
    /** 
     * get the gateway object
     */
    function get_gateway($gateway='amazon-checkout') {
            global $woocommerce;
            $gateways = $woocommerce->payment_gateways->payment_gateways();
            if(isset($gateways[$gateway])) return $gateways[$gateway];
            return (object) null;
    } 

    function load_assets() {

        if(is_cart() || $this->is_amazon_url()) {

            $js_params = array(); 

            $js_params['merchantId'] = $this->get_gateway()->get_option('merchant_id');

            $widget_url = sprintf(
                'https://static-eu.payments-amazon.com/cba/js/%s/PaymentWidgets.js',
                $this->get_gateway()->get_option('region'));

            if($this->get_gateway()->get_option('environment') == 'test') {
                $widget_url = sprintf(
                    'https://static-eu.payments-amazon.com/cba/js/%s/sandbox/PaymentWidgets.js',
                    $this->get_gateway()->get_option('region'));
            }

            // load the widgets JS
            wp_enqueue_script(
                'amazon-checkout-widgets', 
                $widget_url,
                array());

            // embed our JS file that makes the AJAX requests
            wp_enqueue_script(
                'amazon-checkout', 
                plugin_dir_url(dirname(__FILE__)) . 'assets/js/amazon-checkout.js', 
                array('amazon-checkout-widgets', 'jquery'),
                $in_footer=false);

            // for cart shipping
            global $woocommerce;
            $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
            $frontend_script_path = $woocommerce->plugin_url() . '/assets/js/frontend/';
            wp_enqueue_script( 'wc-cart', $frontend_script_path . 'cart' . $suffix . '.js', array( 'jquery' ), $woocommerce->version, true );

            // styles for amazon checkout
            wp_register_style('amazon-checkout', plugin_dir_url(dirname(__FILE__)) . 'assets/css/amazon-checkout.css', array(), WC_Amazon_Checkout::VERSION);
            wp_enqueue_style('amazon-checkout');

            // declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
            $js_params['ajaxurl'] = admin_url('admin-ajax.php');

            wp_localize_script(
                'amazon-checkout', 
                'amazonCheckout', 
                $js_params);
        }

    }

    function is_amazon_url($url=null) {
        if(is_null($url)) $url = $this->get_current_url(); 
        return rtrim($this->get_amazon_url(), '/') == rtrim($url, '/');
    }

    function get_protocol() {
        return isset($_SERVER['HTTPS']) ? 'https' : 'http';
    }

    function get_current_url() {
        $protocol = $this->get_protocol();
        $url = parse_url($_SERVER['REQUEST_URI']);
        return $protocol . '://'.$_SERVER['HTTP_HOST'].$url['path'];
    }

    function get_amazon_url() {
        return home_url(WC_AMAZON_CHECKOUT::URL, $this->get_protocol());
    }

    function format_cart($purchase_contract_id, $merchant_id, $order) {

        $cart= array();

        /*
         * Amazon accepts "PurchaseItems" in this format:
         *
        &PurchaseItems.PurchaseItem.1.MerchantItemId=1
        &PurchaseItems.PurchaseItem.1.SKU=Item1SKU
        &PurchaseItems.PurchaseItem.1.MerchantId=A32EMVWCF1111H
        &PurchaseItems.PurchaseItem.1.Title=Item1Title
        &PurchaseItems.PurchaseItem.1.Description=ItemDescription
        &PurchaseItems.PurchaseItem.1.UnitPrice.Amount=3.14
        &PurchaseItems.PurchaseItem.1.UnitPrice.CurrencyCode=GBP
        &PurchaseItems.PurchaseItem.1.Quantity=1
        &PurchaseItems.PurchaseItem.1.URL=http%3A%2F%2Fmydomain.com%2Fproduct
        &PurchaseItems.PurchaseItem.1.Category=books
        &PurchaseItems.PurchaseItem.1.FulfillmentNetwork=MERCHANT
        &PurchaseItems.PurchaseItem.1.ItemCustomData=MHhkZWFkYmVlZg%3D%3D
        &PurchaseItems.PurchaseItem.1.ProductType=PHYSICAL
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.Weight.Value=3.25
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.Weight.Unit=KG
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.Condition=new
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.DeliveryMethod.ServiceLevel=Standard
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.DeliveryMethod.DisplayableShippingLabel=One%20Day%20Super%20Saver%20Shipping
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.DeliveryMethod.DestinationName=Item1Destination
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.DeliveryMethod.ShippingCustomData=MHhkZWFkYmVlZg%3D%3D
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.ItemCharges.Shipping.Amount=3.14
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.ItemCharges.Shipping.CurrencyCode=GBP
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.1.PromotionId=EXAMPLE
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.1.Description=EXAMPLE
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.1.Discount.Amount=3.14
        &PurchaseItems.PurchaseItem.1.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.1.Discount.CurrencyCode=GBP
        *
        **/

        $items = $order->get_items(); 
        error_log('ITEMs');
        error_log(var_export($items, true));
        $i = 0;
        if(count($items)>0) { 
            foreach ($items as $item) {
                $i++;
                $product = $order->get_product_from_item($item);
                error_log('PRODUCT.'.$i);
                error_log(var_export($product, true));
                error_log('PRODUCT.'.$i.' ATTRS');
                error_log(var_export($product->get_attributes(), true));
                error_log('PRODUCT.'.$i.' ITEM');
                error_log(var_export($item, true));

                $prefix ='PurchaseItems.PurchaseItem.'.$i;

                $cart[$prefix.'.MerchantItemId']=isset($item['product_id']) ? $item['product_id'] : $item['id'];
                $sku=($product->get_sku()) ? $product->get_sku() : $cart[$prefix.'.MerchantItemId'];
                $cart[$prefix.'.SKU']=substr($sku, 0, 40);
                $cart[$prefix.'.MerchantId']=$merchant_id;
                $cart[$prefix.'.Title']=substr($item['name'], 0, 80);

                $cart[$prefix.'.Description']='';
                if($product && property_exists($product, 'post')) {
                    if($post = $product->post) {
                        if(property_exists($post, 'post_content')) {
                            $cart[$prefix.'.Description']=$post->post_content;
                        }
                        $cart[$prefix.'.URL']= get_permalink($post);
                    }
                }
                $cart[$prefix.'.URL']=substr($cart[$prefix.'.URL'], 0, 2000);
                $cart[$prefix.'.Description']=substr($cart[$prefix.'.Description'], 0, 200);

                $cart[$prefix.'.UnitPrice.Amount']=$product->get_price();
                $cart[$prefix.'.UnitPrice.CurrencyCode']=get_woocommerce_currency();
                $cart[$prefix.'.Quantity']=$item['qty'];

                $cart[$prefix.'.Category']='';
                $cart[$prefix.'.FulfillmentNetwork']='MERCHANT';
                $cart[$prefix.'.ItemCustomData']='';
                $cart[$prefix.'.ProductType']=$product->is_virtual() || $product->is_downloadable() ? '' : 'PHYSICAL';

                /* 
                 * amazon likes weight as lbs or kgs 
                 * TODO - do conversions
                 * until then comment out cos it's not required
                 */
                //$cart[$prefix.'.PhysicalProductAttributes.Weight.Value']=intval($product->get_weight());
                //$cart[$prefix.'.PhysicalProductAttributes.Weight.Unit']=get_option('woocommerce_weight_unit');

                /*
                // TODO unrequired fields
                $cart[$prefix.'.PhysicalProductAttributes.Condition']=new;
                $cart[$prefix.'.PhysicalProductAttributes.DeliveryMethod.ServiceLevel']=Standard;
                $cart[$prefix.'.PhysicalProductAttributes.DeliveryMethod.DisplayableShippingLabel']=One%20Day%20Super%20Saver%20Shipping;
                $cart[$prefix.'.PhysicalProductAttributes.DeliveryMethod.DestinationName']=Item1Destination;
                $cart[$prefix.'.PhysicalProductAttributes.DeliveryMethod.ShippingCustomData']=MHhkZWFkYmVlZg%3D%3D;
                $cart[$prefix.'.PhysicalProductAttributes.ItemCharges.Shipping.Amount']=3.14;
                $cart[$prefix.'.PhysicalProductAttributes.ItemCharges.Shipping.CurrencyCode']=GBP;
                $cart[$prefix.'.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.1.PromotionId']=EXAMPLE;
                $cart[$prefix.'.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.1.Description']=EXAMPLE;
                $cart[$prefix.'.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.1.Discount.Amount']=3.14;
                $cart[$prefix.'.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.1.Discount.CurrencyCode']=GBP;
                */
            }
        }
        error_log('CART');
        error_log(var_export($cart, true));
        return $cart;
    }


    /*
    Charges.Promotions.Promotion.1.PromotionId=EXAMPLE
    Charges.Promotions.Promotion.1.Description=EXAMPLE
    Charges.Promotions.Promotion.1.Discount.Amount=3
    Charges.Promotions.Promotion.1.Discount.CurrencyCode=GBP
    */

    function cart_has_downloadables() {
        global $woocommerce;
        foreach($woocommerce->cart->get_cart() as $cart_item_key => $values ) {
            $_product = $values['data'];
            if($_product->is_virtual() || $_product->is_downloadable()) {
                return true;
            }
        }
        return false;
    }

} // end WC_Amazon_Checkout_Helper class

