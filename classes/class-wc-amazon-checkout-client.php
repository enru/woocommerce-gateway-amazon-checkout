<?php
/**
 * bare bones client for amazon checkout API
 * e.g.

$client = new WC_Amazon_Checkout_Client($accessKey, $secretKey, $cbaServiceURL);
$purchaseContractId = 'amzn1.contract.1.1.2.23bc485cc4c6b0dc63cd7f8c0d3d8900';
$result = $client->httpPost(array(
    'Action'=>'GetPurchaseContract',
    'PurchaseContractId'=>$purchaseContractId,
));

 *
 **/

class WC_Amazon_Checkout_Client  {

    protected $_awsAccessKeyId = null;
    protected $_awsSecretAccessKey = null;
    protected $_version = null;
    protected $_endpoint = null;

    protected $_config = array(
        'ServiceURL' => null,
        'UserAgent' => 'CBA_CheckoutAPI_Inline/1.1 (Language=PHP; ReleaseDate=10_2012)',
        'SignatureVersion' => 2,
        'SignatureMethod' => 'HmacSHA256',
        'ProxyHost' => null,
        'ProxyPort' => -1,
        'MaxErrorRetry' => 3,
    );

    /**
     * Construct new Client
     *
     * @param string $awsAccessKeyId AWS Access Key ID
     * @param string $awsSecretAccessKey AWS Secret Access Key
     * @param array $config configuration options.
     * Valid configuration options are:
     * <ul>
     * <li>ServiceURL</li>
     * <li>UserAgent</li>
     * <li>SignatureVersion</li>
     * <li>TimesRetryOnError</li>
     * <li>ProxyHost</li>
     * <li>ProxyPort</li>
     * <li>MaxErrorRetry</li>
     * </ul>
     */
    public function __construct($awsAccessKeyId, $awsSecretAccessKey, $serviceURL) {

        iconv_set_encoding('output_encoding', 'UTF-8');
        iconv_set_encoding('input_encoding', 'UTF-8');
        iconv_set_encoding('internal_encoding', 'UTF-8');

        $this->_awsAccessKeyId = $awsAccessKeyId;
        $this->_awsSecretAccessKey = $awsSecretAccessKey;

        $config = array(
            'ServiceURL' => $serviceURL,
            'Version' => $this->_version,
        );

        $this->_config = array_merge($this->_config, $config);
    }

    /**
     * Perform HTTP post with exponential retries on error 500 and 503
     *
     */
    public function httpPost(array $parameters, $uri='', $data=null) {

        // set the endpoint b4 we do anything else
        $this->_endpoint = rtrim($this->_config['ServiceURL'],'/').'/'.ltrim($uri,'/');

        $parameters = $this->addRequiredParameters($parameters);

        $post_data = http_build_query($parameters, '', '&'); 

        $headers = array();

        if(!is_null($data)) { 
            $this->_endpoint .= '?'.$post_data;
            $post_data = $data; 
            $headers[] = 'Content-MD5: ' . base64_encode(md5($data, true));
        }

        //initialize CURL
        $curlHandle = curl_init();

        //compose CURL request
        curl_setopt($curlHandle, CURLOPT_URL, $this->_endpoint);
        curl_setopt($curlHandle, CURLOPT_USERAGENT, $this->_config['UserAgent']);
        curl_setopt($curlHandle, CURLOPT_POST, true);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($curlHandle, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_CAINFO, AMAZON_CHECKOUT_DIR.'/ca-bundle.crt');
        curl_setopt($curlHandle, CURLOPT_CAPATH, AMAZON_CHECKOUT_DIR.'/ca-bundle.crt');
        curl_setopt($curlHandle, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($curlHandle, CURLOPT_MAXREDIRS, 0);
        curl_setopt($curlHandle, CURLOPT_HEADER, true);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_NOSIGNAL, true);
        curl_setopt($curlHandle, CURLOPT_HEADER, false);

        if(count($headers) > 0) {
            curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $headers);
        }

        // Execute the request
        $responseBody = curl_exec($curlHandle);
        $info = curl_getinfo($curlHandle);

        // to grab the response code only from the Header
        $code = $info["http_code"];

        //close the CURL conection
        curl_close($curlHandle);

        error_log(var_export($code, true));
        error_log(var_export($responseBody, true));

        return array ('Status' => (int)$code, 'ResponseBody' => $responseBody);
    }

    /**
     * Computes RFC 2104-compliant HMAC signature.
     */
    protected function sign($data, $key, $algorithm) {

        switch($algorithm) {
            case 'HmacSHA1' :
                $hash = 'sha1';
                break;
            case 'HmacSHA256' :
                 $hash = 'sha256';
                 break;
            default :
                throw new Exception ("Non-supported signing method specified");
        }
        
        return base64_encode(
            hash_hmac($hash, $data, $key, true));
    }


    /**
     * Formats date as ISO 8601 timestamp
     */
    protected function getFormattedTimestamp() {
        return gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
    }

    /**
     * Add authentication related and version parameters
     */
    protected function addRequiredParameters(array $parameters) {
        $parameters['AWSAccessKeyId'] = $this->_awsAccessKeyId;
        $parameters['Timestamp'] = $this->getFormattedTimestamp();
        $parameters['Version'] = $this->_version;
        $parameters['SignatureVersion'] = $this->_config['SignatureVersion'];
        if ($parameters['SignatureVersion'] > 1) {
            $parameters['SignatureMethod'] = $this->_config['SignatureMethod'];
        }
        $parameters['Signature'] = $this->signParameters($parameters, $this->_awsSecretAccessKey);
        error_log(var_export($parameters, true));

        return $parameters;
    }

    /**
     * Computes RFC 2104-compliant HMAC signature for request parameters
     * Implements AWS Signature, as per following spec:
     *
     * If Signature Version is 0, it signs concatenated Action and Timestamp
     *
     * If Signature Version is 1, it performs the following:
     *
     * Sorts all  parameters (including SignatureVersion and excluding Signature,
     * the value of which is being created), ignoring case.
     *
     * Iterate over the sorted list and append the parameter name (in original case)
     * and then its value. It will not URL-encode the parameter values before
     * constructing this string. There are no separators.
     *
     * If Signature Version is 2, string to sign is based on following:
     *
     *    1. The HTTP Request Method followed by an ASCII newline (%0A)
     *    2. The HTTP Host header in the form of lowercase host, followed by an ASCII newline.
     *    3. The URL encoded HTTP absolute path component of the URI
     *       (up to but not including the query string parameters);
     *       if this is empty use a forward '/'. This parameter is followed by an ASCII newline.
     *    4. The concatenation of all query string components (names and values)
     *       as UTF-8 characters which are URL encoded as per RFC 3986
     *       (hex characters MUST be uppercase), sorted using lexicographic byte ordering.
     *       Parameter names are separated from their values by the '=' character
     *       (ASCII character 61), even if the value is empty.
     *       Pairs of parameter and values are separated by the '&' character (ASCII code 38).
     *
     */
    protected function signParameters(array $parameters, $key) {
        $stringToSign = $this->calculateStringToSignV2($parameters);
        return $this->sign($stringToSign, $key, $this->_config['SignatureMethod']);
    }


    /**
     * Calculate String to Sign for SignatureVersion 2
     * @param array $parameters request parameters
     * @return String to Sign
     */
    protected function calculateStringToSignV2(array $parameters) {

        // sort the parameter before signing
        uksort($parameters, 'strcmp');

        // get the endpoint host & URI parts
        $endpoint = parse_url($this->_endpoint);
        $uri = array_key_exists('path', $endpoint) ? $endpoint['path'] : null;
        if (!isset ($uri)) $uri = "/"; 
		$uriencoded = implode("/", array_map(array($this, "_urlencode"), explode("/", $uri)));

        // add it together
        $data = 'POST'."\n";
        $data .= $endpoint['host']."\n";
        $data .= $uriencoded."\n";
        $data .= $this->getParametersAsString($parameters);
        return $data;
    }

    /**
     * Convert paremeters to Url encoded query string
     */
    protected function getParametersAsString(array $parameters) {
        $queryParameters = array();
        foreach ($parameters as $key => $value) {
            $queryParameters[] = $key . '=' . $this->_urlencode($value);
        }
        return implode('&', $queryParameters);
    }

    /** surely we can do better than this? */
    protected function _urlencode($value) {
        return str_replace('%7E', '~', rawurlencode($value));
    }

}

