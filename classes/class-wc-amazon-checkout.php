<?php
require_once 'class-wc-amazon-checkout-helper.php';
require_once 'class-wc-amazon-checkout-client-cba.php';
require_once 'class-wc-amazon-checkout-client-mws.php';
/**
* Gateway class
*/
class WC_Amazon_Checkout extends WC_Payment_Gateway {

    private static $regions =  array(
        'gb' => 'United Kingdon (gb)',
        'de' => 'Germany (de)',
    );

    const URL = '/amazon/checkout/';
    const VERSION = 1.0;

    // live mode by default
    private $cba_service_tld = 'https://payments.amazon.co.uk/';
    private $mws_service_tld = 'https://mws.amazonservices.co.uk/';

    private $cba_service_uri = 'cba/api/purchasecontract/';
    private $mws_service_uri = '';

    private $cba_service_url = '';
    private $mws_service_url = '';

    public function get_option( $key, $empty_value = '' ) {
    
        if ( empty( $this->settings ) ) $this->init_settings();
        
        // Get option default if unset
        if ( ! isset( $this->settings[ $key ] ) ) {
            $this->settings[ $key ] = isset( $this->form_fields[ $key ]['default'] ) ? $this->form_fields[ $key ]['default'] : '';
        }
        
        if ( empty( $this->settings[ $key ] ) ) $this->settings[ $key ] = $empty_value;
        
        return $this->settings[ $key ];
    }

    public function __construct() {

        $this->helper = new WC_Amazon_Checkout_Helper();

        $this->id = 'amazon-checkout';
        $this->method_title = 'Amazon Payments EU';
        $this->title = 'Amazon Payments EU';
        $this->icon = WP_PLUGIN_URL . "/" . plugin_basename( dirname(dirname(__FILE__))) . '/images/amazon.png';
        $this->has_fields = true;
        
        // load form fields
        $this->init_form_fields();

        // load settings
        $this->init_settings();

        switch($this->get_option('region')) {
            case 'de': 
                $this->cba_service_tld = 'https://payments.amazon.de/'; 
                break;
            case 'uk': 
            default:
                $this->cba_service_tld = 'https://payments.amazon.co.uk/';
                break;
        }

        // Amazon Checkout Sandbox Service URL
        if($this->get_option('environment') == 'test') {
            switch($this->get_option('region')) {
                case 'de': 
                    $this->cba_service_tld = 'https://payments-sandbox.amazon.de/'; 
                    break;
                case 'uk': 
                default:
                    $this->cba_service_tld = 'https://payments-sandbox.amazon.co.uk/';
                    break;
            }
        }

        $this->cba_service_url = $this->cba_service_tld . $this->cba_service_uri;
        $this->mws_service_url = $this->mws_service_tld . $this->mws_service_uri;


        // Amazon Checkout API client
        $this->cba_client = new WC_Amazon_Checkout_Client_CBA($this->get_option('access_key'), $this->get_option('secret_key'), $this->cba_service_url);
        $this->mws_client = new WC_Amazon_Checkout_Client_MWS($this->get_option('access_key'), $this->get_option('secret_key'), $this->mws_service_url);

        // set up the JavaScript/CSS for this plugin
        add_action('wp', array($this->helper, 'load_assets'));

        // make sure we get shipping address details
        update_option('woocommerce_require_shipping_address', 'yes');

        // actions
        /* 1.6.6 */
        add_action( 'woocommerce_update_options_payment_gateways', array( $this, 'process_admin_options' ) );

        /* 2.0.0 */
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

        add_action('woocommerce_proceed_to_checkout', array($this, 'checkout_button')); 

        // ajax actions
        add_action('wp_ajax_amazon_checkout_get_address', array($this, 'get_address'), 10, 1);
        add_action('wp_ajax_nopriv_amazon_checkout_get_address', array($this, 'get_address'), 10, 1);
        add_action('wp_ajax_amazon_checkout_get_address_from_amazon', array($this, 'get_address_from_amazon'), 10);

        // check if the route is for amazon checkout
        add_filter('parse_request', array($this, 'check_route')); 

        // clear fields not given by amazon
        add_filter('woocommerce_checkout_fields', array($this, 'clear_required_fields'));

        // auto import/sync orders
        add_action("get_orders", array($this, 'get_orders'));
        add_action("wp_loaded", array($this, 'run_scheduled_events'));

        // hook into order completion
        add_action('woocommerce_order_status_completed', array($this, 'complete_order'), 10, 1);

        // enable guest checkout
        add_filter('pre_option_woocommerce_enable_guest_checkout', array($this, 'enable_guest_checkout'), 10, 1);

        // disable woocommerce_ship_to_billing_address_only'
        add_filter('pre_option_woocommerce_ship_to_billing_address_only', array($this, 'not_billing_address_only'), 10, 1);

        // make billing fields optional
        add_filter('woocommerce_billing_fields', array($this, 'woocommerce_billing_fields'), 10, 2);
        
    }

    /**
     * adds 'full-width' class to checkout <body> tag
     */
    function full_width_checkout($classes) {
        $classes[] = 'full-width';
        return $classes;
    }

    /**
     * check the selected WooCommerce currency is correct for selected region
     */
    function check_currency() {
        $currency = get_woocommerce_currency();
        switch($this->get_option('region')) {
            case 'de': 
                if($currency != 'EUR') {
                    echo '<div class="error">';
                    echo '<p>Amazon Payments EU in Germany requires EURO currency</p>';
                    echo '</div>';
                }
                break;
            case 'uk': 
            default:
                if($currency != 'GBP') {
                    echo '<div class="error">';
                    echo '<p>Amazon Payments EU in UK requires GBP currency</p>';
                    echo '</div>';
                }
                break;
        }
    }

    /**
     * schedule events like email sendouts
     */
    function run_scheduled_events() {
        //echo "<pre>".var_export(_get_cron_array(), true);
        if(!wp_next_scheduled('get_orders')) {
            wp_schedule_event(time(), $this->get_option('update_period'), 'get_orders');
        }
    }

    /**
     * reschedule when saving options
     */
    function process_admin_options() {
    	// Save regular options
    	parent::process_admin_options();
        // Get the timestamp for the next event.
        $timestamp = wp_next_scheduled('get_orders');
        wp_unschedule_event($timestamp, 'get_orders'); 
        wp_reschedule_event(time(), $_POST['woocommerce_amazon-checkout_update_period'], 'get_orders');
    }

    /**
    * Initialise Gateway Settings Form Fields
    */
    function init_form_fields() {

        //  array to generate admin form
        $this->form_fields = array(
            'enabled' => array(
                'title' => __( 'Enable/Disable', 'amazoncheckout' ), 
                'type' => 'checkbox', 
                'label' => __( 'Enable Amazon Payments EU', 'amazoncheckout' ), 
                'default' => 'yes'
            ), 
            'title' => array(
                'title' => __( 'Title', 'amazoncheckout' ), 
                'type' => 'text', 
                'description' => __( 'This is the title displayed to the user during checkout.', 'amazoncheckout' ), 
                'default' => __( 'Amazon Payments EU', 'amazoncheckout' )
            ),
            'description' => array(
                'title' => __( 'Description', 'amazoncheckout' ), 
                'type' => 'textarea', 
                'description' => __( 'This is the description which the user sees during checkout.', 'amazoncheckout' ), 
                'default' => __("Payment via Amazon Payments EU, you can pay by credit or debit card", 'amazoncheckout')
            ),
            'merchant_id' => array(
                'title' => __( 'Merchant ID', 'amazoncheckout' ), 
                'type' => 'text', 
                'description' => __( 'Amazon Merchant ID.', 'amazoncheckout' ), 
                'default' => '',
            ),
            'access_key' => array(
                'title' => __( 'Access Key', 'amazoncheckout' ), 
                'type' => 'text', 
                'description' => __( 'Amazon Access Key.', 'amazoncheckout' ), 
                'default' => '',
            ),
            'secret_key' => array(
                'title' => __( 'Secret Key', 'amazoncheckout' ), 
                'type' => 'text', 
                'description' => __( 'Amazon Secret Key.', 'amazoncheckout' ), 
                'default' => '',
            ),
            'environment' => array(
                'title' => __('Mode', 'amazoncheckout'),
                'type' => 'select',
                'options' => array(
                    'test' => 'Test', 
                    'live' => 'Live',
                ),            
                'description' => __( 'Select Test or Live environment.', 'amazoncheckout' ),
                'default' => 'live',
            ),
            'update_period' => array(
                'title' => __('Update Period', 'amazoncheckout'),
                'type' => 'select',
                'options' => array(
                    'hourly' => 'Hourly', 
                    'twicedaily' => 'Twice Daily',
                    'daily' => 'Daily',
                ),            
                'description' => __( 'How often should new order details be fetched from Amazon?', 'amazoncheckout' ),
                'default' => 'hourly',
            ),
            'region'  =>  array(
                'title' => __('Region', 'amazoncheckout'),
                'type' => 'select',
                'options' => WC_Amazon_Checkout::$regions,
                'description' => __( 'Select the region you are using for this payment gateway.', 'amazoncheckout' ),
                'default' => 'gb',
            )
        );

        /*
        $this->form_fields['mobile'] = array(
            'title' => __( 'Use on mobiles/cell/phones', 'amazoncheckout' ), 
            'type' => 'checkbox', 
            'label' => __( 'Yes', 'amazoncheckout' ), 
            'default' => 'no'
        );

        $this->form_fields['debug'] = array(
            'title' => __( 'Debug', 'amazoncheckout' ), 
            'type' => 'checkbox', 
            'label' => __( 'Enable logging ', 'amazoncheckout' ), 
            'default' => 'no'
        );
        */

    } // End init_form_fields()

    /**
    * Admin Panel Options 
    * - Options for bits like 'title' and availability on a country-by-country basis
    *
    * @since 1.0.0
    */
    public function admin_options() {
        ?>
        <h3><?php _e('Amazon Checkout Payment', 'amazoncheckout'); ?></h3>
        <p><?php _e('Allows Payment to be taken using the Amazon Checkout Payment Gateway', 'amazoncheckout'); ?></p>
        <table class="form-table">
        <?php
        // Generate the HTML For the settings form.
        $this->generate_settings_html();
        ?>
        </table><!--/.form-table-->
        <?php
    } // End admin_options()


    /** 
     * validate details before sending to amazon
     */
    private function _validated() {
        global $woocommerce; 

        /*
        // check SKUs
        foreach($woocommerce->cart->get_cart() as $cart_item_key => $values) {
            $_product = get_product( $values['variation_id'] ? $values['variation_id'] : $values['product_id'] );
            error_log(var_export($_product, true));
            if(strlen($_product->get_sku()) > 40) return false;
        }
        */
        return true;

    }

    /**
    * process payment
    * 
    * on Amazon Checkout API do:
    *
    * 1. Send the line items and prices
    * 2. Send delivery costs and promotions
    * 3. Confirm order and close the order session
    *
    * @param int $order_id
    */
    function process_payment($order_id) {
        global $woocommerce;

        //$purchase_contract_id = $woocommerce->session->amazon_checkout_purchase_contract_id;
        $purchase_contract_id = $this->get_purchase_contract_id();

        /* don't send to amazon if not a valid cart/order */
        if(false == $this->_validated()) {
            $woocommerce->add_error(__('Payment error:', 'amazoncheckout'));
            return array(
                'result' => 'error',
                'message' => 'Payment Error',
            );
        }                    

        // create an order
        $order = new WC_Order($order_id);
        //$order->update_status('on-hold', __('Awaiting Amazon Contract', 'amazoncheckout'));

        try {
            $cart = $this->helper->format_cart($purchase_contract_id, $this->get_option('merchant_id'), $order);
            $this->cba_client->set_purchase_items($purchase_contract_id, $cart);

            $shipping_amt = $order->get_shipping();
            error_log('shipping:'.$shipping_amt);

            $discount_amt = $order->get_total_discount();
            error_log('discount:'.$discount_amt);

            $this->cba_client->set_contract_charges($purchase_contract_id, $currency=get_woocommerce_currency(), $shipping_amt, $discount_amt);

            if($amazon_order_id = $this->cba_client->complete_purchase_contract($purchase_contract_id)) {
                update_post_meta($order->id, 'amazon_checkout_order_id', $amazon_order_id);
                $order->add_order_note(__('Amazon Checkout Order Id: '). $amazon_order_id);
            }
            update_post_meta($order->id, 'amazon_checkout_details_reqd', 1);

        }
        catch(Exception $e) {
            $order->cancel_order(__('Problem checking out on Amazon:', 'amazoncheckout')."\n$e");
            $woocommerce->add_error(__('Payment error:', 'amazoncheckout') . $e);
            return array(
                'result' => 'error',
                'message' => $e,
            );
        }

        //$order->update_status('on-hold', __('Awaiting Amazon Shipment', 'amazoncheckout'));

        /* don't complete payment or reduce stock until order has been marked as 'Ready to Ship' */

        // Remove cart
        $woocommerce->cart->empty_cart();

        // Clear Amazon session
        //unset($woocommerce->session->amazon_checkout_purchase_contract_id);
        $this->set_purchase_contract_id(null);

        // Return thank you page redirect
        return array(
            'result' => 'success',
            'redirect' => $this->get_return_url( $order )
        );

    } // end process_payment

    /**
     * Amazon Checkout button
     * displays amazon checkout button
     */
    function checkout_button() { 
        // dont show if cart has downloadable products - amazon doesnt allow
        // only show button is this gateway is enabled
        if(false == $this->helper->cart_has_downloadables() && 'yes' == $this->get_option('enabled')):
        ?>
<div id="AmazonInlineCheckoutWidget"></div>
<script>
new CBA.Widgets.InlineCheckoutWidget({
    merchantId: "<?php echo esc_js($this->get_option('merchant_id')); ?>",
    design: { size: { width:'126', height:'24' } },
    onAuthorize: function(widget) {
        window.location = '<?php echo site_url('/amazon/checkout'); ?>/?purchaseContractId='+widget.getPurchaseContractId();
    }
}).render("AmazonInlineCheckoutWidget");
</script>
<?php 
        endif;
    }

    /**
     * Router 
     * checks given URL to see if this plugin should handle it
     **/
    function check_route() {

        // handle if it's our amazon checkout URL
        if($this->helper->is_amazon_url()) {

            global $woocommerce;
            if(is_null($woocommerce) && !class_exists('Woocommerce')) {
                require_once(plugin_dir_path('woocommerce.php'));
                $woocommerce = new Woocommerce();
            }

            $this->helper->load_assets();

            global $woocommerce;

            // if this is a callback the purchaseContractId (after login) will be set
            // redirect back to a clean checkout URL 
            if(isset($_GET['purchaseContractId'])) {
                $purchase_contract_id = filter_var($_GET['purchaseContractId'], FILTER_SANITIZE_SPECIAL_CHARS); 
                //$woocommerce->session->amazon_checkout_purchase_contract_id = $purchase_contract_id;
                $this->set_purchase_contract_id($purchase_contract_id);
                wp_redirect(home_url('/amazon/checkout'));
            }

            // if the contract ID is set in the session and we got a POST to place the order...
            //if($purchase_contract_id = $woocommerce->session->amazon_checkout_purchase_contract_id) {
            if($purchase_contract_id = $this->get_purchase_contract_id()) {
                if(isset($_POST['amazon_checkout_place_order'])) {
                    // set teh address details from the purchaseContractId
                    // and set them in the POST data so woocommerce can process them
                    if($address = $this->get_address($purchase_contract_id)) {
                        error_log('Got address from Amazon:');
                        error_log(var_export($address, true));
                        if(property_exists($address, 'City')) $_POST['shipping_city'] = (string) $address->City;
                        if(property_exists($address, 'CountryCode')) $_POST['shipping_country'] = (string) $address->CountryCode;
                        if(property_exists($address, 'StateOrProvinceCode')) $_POST['shipping_state'] = (string) $address->StateOrProvinceCode;
                        if(property_exists($address, 'PostalCode')) $_POST['shipping_postcode'] = (string) $address->PostalCode;
                        // WC 2.1.5
                        $_POST['ship_to_different_address'] = true;
                    }
                    // checkout using woocommerce
                    $woocommerce_checkout = $woocommerce->checkout();
                    $woocommerce_checkout->process_checkout();
                    if($woocommerce->error_count() > 0) {
                        wp_redirect(home_url('/amazon/checkout'));
                    }
                    die();
                }
            }

            // make the checkout full width
            add_filter('body_class', array($this, 'full_width_checkout'));

            $this->get_orders();

            // get teh checkout template, buffer & echo the result
			ob_start();
            include AMAZON_CHECKOUT_DIR . '/templates/amazon-checkout.php';
			ob_end_flush();
			die();
        }
    }

    /**
     * AJAX function
     * uses Amazon Checkout API client to get customer's address
     **/
    function get_address($purchase_contract_id=null) {
        // TODO: nonce
        //check_ajax_referer( 'my-special-string', 'security' );
        if(!$purchase_contract_id && isset($_POST['purchase_contract_id'])) {
            $purchase_contract_id = filter_var($_POST['purchase_contract_id'], FILTER_SANITIZE_SPECIAL_CHARS); 
        }
        if($purchase_contract_id) {
            if($address = $this->cba_client->get_address($purchase_contract_id)) {
                if(is_ajax()) {
                    header('Content-Type: application/json');
                    echo json_encode($address);
                    die();
                }
                return $address;
            }
        }
    }

    /**
     * filter for 'woocommerce_checkout_fields'
     * clears requirement for all fields except some shipping fields
     * this is all we get from amazon
     **/
    function clear_required_fields($fields) {

        if(isset($_POST['amazon_checkout_place_order'])) {

            $required_fields = array(
                'shipping_city',
                //'shipping_state',
                'shipping_country',
                'shipping_postcode',
            );

            foreach ( $fields as $fieldset_key => $fieldset ) {
                foreach ( $fieldset as $key => $field ) {
                    // ignore account fields etc
                    if(!isset($field['required'])) continue;
                    // most are NOT required 
                    $fields[$fieldset_key][$key]['required'] = false;
                    if(in_array($key, $required_fields)) {
                        $fields[$fieldset_key][$key]['required'] = true;
                    }
                }
            }

        }

        return $fields;
    }

    /*
     * gets order details from amazon
    */
    function get_orders() {

        //global $woocommerce;
        
        $args = array(
            'post_type' => 'shop_order',
            'post_status' => 'publish',
            'nopaging' => true,
            'meta_key' => 'amazon_checkout_details_reqd',
            'meta_value' => 1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'shop_order_status',
                    'field' => 'slug',
                    'terms' => array('pending', 'processing','on-hold')
                )
             )
        );

        $loop = new WP_Query($args);
        error_log('updating order details frm amazon');
        
        while($loop->have_posts()) {
            // set up the WC order
            $loop->the_post();
            $order_id = $loop->post->ID;
            $order = new WC_Order($order_id);

            // get the Amazon order details
            $amazon_checkout_order_id = get_post_meta($order_id, 'amazon_checkout_order_id', true);
            if($order_details = $this->mws_client->get_order($amazon_checkout_order_id, $this->get_option('merchant_id'))) {

                // update the WC order with data frm Amazon
                $name = $order_details['Name'];
                $first_name = substr($name, 0, strpos($name, ' '));
                $last_name = substr($name, strpos($name, ' '));
                update_post_meta($order_id, '_shipping_first_name', $first_name);
                update_post_meta($order_id, '_shipping_last_name', $last_name);
                update_post_meta($order_id, '_shipping_address_1', $order_details['AddressLine1']);
                update_post_meta($order_id, '_shipping_postcode', $order_details['PostalCode']);
                update_post_meta($order_id, '_shipping_country', $order_details['CountryCode']);
                update_post_meta($order_id, '_shipping_state', $order_details['StateOrRegion']);
                update_post_meta($order_id, '_shipping_city', $order_details['City']);
                update_post_meta($order_id, '_shipping_phone', $order_details['Phone']);
                update_post_meta($order_id, 'shipping_phone', $order_details['Phone']);

                // finish this order update, unmark as reqd & move to processing status
                delete_post_meta($order_id, 'amazon_checkout_details_reqd');
                $order->update_status('processing', __('Awaiting Shipment', 'amazoncheckout'));
            }

        }

    }

    /**
     * this is where we complete the order and notify amazon
     */
    function complete_order($order_id) {
        error_log('completing '.$order_id);
        $this->mws_client->confirm_shipment($order_id, $this->get_option('merchant_id')); 
    }

    /** 
     * returns purchase contract ID from session
     */
    function get_purchase_contract_id() {
        global $woocommerce;
        $purchase_contract_id = null;
        if($woocommerce && property_exists($woocommerce, 'session')) {
            $purchase_contract_id = $woocommerce->session->amazon_checkout_purchase_contract_id;
        }
        elseif(isset($_SESSION['amazon_checkout']['purchase_contract_id'])) {
            $purchase_contract_id = $_SESSION['amazon_checkout']['purchase_contract_id'];
        }
        return $purchase_contract_id;
    }

    /** 
     * set purchase contract ID in the session
     */
    function set_purchase_contract_id($purchase_contract_id) {
        global $woocommerce;
        if($woocommerce && property_exists($woocommerce, 'session')) {
            $woocommerce->session->amazon_checkout_purchase_contract_id = $purchase_contract_id;
        }
        else {
            $_SESSION['amazon_checkout']['purchase_contract_id'] = $purchase_contract_id; 
        }
    }

    /** 
     * gets order address details from amazon
     * called in order screen in admin panel
     */
    function get_address_from_amazon() {
        if(isset($_POST['post_id'])) {
            check_ajax_referer('get-customer-details', 'security');
            $order_id = $_POST['post_id'];
            $order = new WC_Order($order_id);
            if('amazon-checkout' == $order->payment_method) {
                if($amazon_checkout_order_id = get_post_meta($order_id, 'amazon_checkout_order_id', true)) {
                    if($address = $this->mws_client->get_order($amazon_checkout_order_id, $this->get_option('merchant_id'))) {

                        // update the WC order with data frm Amazon
                        $name = $address['Name'];
                        $first_name = substr($name, 0, strpos($name, ' '));
                        $last_name = substr($name, strpos($name, ' '));
                        $address['FirstName'] = $first_name;
                        $address['LastName'] = $last_name;
                        header('Content-Type: application/json');
                        echo json_encode($address);
                        die();
                    }
                }
            }
        }
    }

    /** 
     * makes sure we can use guest checkout
     */
    function enable_guest_checkout($default=false) {
        if($this->helper->is_amazon_url()) {
            $default = 'yes';
        }
        return $default;
    }

    function not_billing_address_only($default=false) {
        if($this->helper->is_amazon_url()) {
            $default = 'no';
        }
        elseif(isset($_POST['action']) && $_POST['action'] == 'woocommerce_update_order_review') {
            $default = 'no';
        }
            //$default = false;
        return $default;
    }


    /**
     * billing fields not required for amazon checkout
     */
    function woocommerce_billing_fields($address_fields, $country) {
        if($this->helper->is_amazon_url()) {
            foreach($address_fields as $field) {
                $field['required'] = false;
            }
        }
        return $address_fields;
    }

} // end WC_Amazon_Checkout class

