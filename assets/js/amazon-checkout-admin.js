jQuery(function($) {

	$('button.get_amazon_address').click(function(){

		var answer = confirm('Get address from Amazon?');
		if (answer){

			// Get post ID to load data for
			var post_id = $('#post_ID').val();

			if (!post_id) {
				alert('Woops! no order');
				return false;
			}

			var data = {
				post_id: 	 post_id,
                action:     'amazon_checkout_get_address_from_amazon',
				security:  woocommerce_writepanel_params.get_customer_details_nonce
			};

			$(this).closest('.edit_address').block({ message: null, overlayCSS: { background: '#fff url(' + woocommerce_writepanel_params.plugin_url + '/assets/images/ajax-loader.gif) no-repeat center', opacity: 0.6 } });

			$.ajax({
				url: woocommerce_writepanel_params.ajax_url,
				data: data,
				type: 'POST',
				success: function( response ) {
					var info = response;

					if (typeof info == 'object') {
						$('input#_shipping_first_name').val( info.FirstName );
						$('input#_shipping_last_name').val( info.LastName );
						$('input#_shipping_company').val( info.Company );
						$('input#_shipping_address_1').val( info.AddressLine1 );
						$('input#_shipping_address_2').val( info.AddressLine2 );
						$('input#_shipping_city').val( info.City );
						$('input#_shipping_postcode').val( info.PostalCode );
						$('#_shipping_country').val( info.CountryCode );
						$('input#_shipping_state').val( info.StateOrRegion );
					}
                    else alert('Nae address frae Amazon :(');

					$('.edit_address').unblock();
				}
			});
		}
		return false;
	});
})
