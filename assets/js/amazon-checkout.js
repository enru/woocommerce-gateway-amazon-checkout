jQuery(function($) {

    // set up the address widget
    new CBA.Widgets.AddressWidget({
        merchantId: amazonCheckout.merchantId,
        displayMode: "Edit",
        onAddressSelect: function(widget) {

            // set up payment widget
            new CBA.Widgets.WalletWidget({
                merchantId: amazonCheckout.merchantId,
                displayMode: "Edit",
                onPaymentSelect: function(widget) {   
                    document.getElementById("place_order").disabled=false;  
                }
            }).render("AmazonWalletWidget");

            // send the contract id to the server to get the address
            $.ajax({
                'url': amazonCheckout.ajaxurl
                ,'type': 'POST'
                ,'data': {
                    'action': 'amazon_checkout_get_address'
                    ,'purchase_contract_id': encodeURIComponent(widget.getPurchaseContractId())
                }
                ,'beforeSend': function() {
                    $('#wc-ac-shipping').html('updating...');
                    
                }
            }).done(function(data) {
                // update shipping details
                $.post(amazonCheckout.ajaxurl, {
                    'action': 'woocommerce_update_order_review'
                    ,'security': $('#update-order-review_n').val()
                    ,'post_data': ''
                    ,'s_city': data['City']
                    ,'s_country': data['CountryCode']
                    ,'s_state': data['StateOrProvinceCode']
                    ,'s_postcode': data['PostalCode']
                }).done(function(data) { 
                    $.post(amazonCheckout.ajaxurl, {
                        'action': 'woocommerce_update_shipping_method'
                        ,'security': $('#update-shipping-method_n').val()
                    }).done(function(data) {
                        // update shipping options
                        $('#wc-ac-shipping').html(data);
                    })
                })
            })
        }
    }).render("AmazonAddressWidget");

 });
