<?php 
global $woocommerce;
get_header(); 
?>
	<div id="primary" class="site-content">
		<div id="content" role="main">
            <div id="amazon-checkout" class="woocommerce">

                <h3><?php _e( 'Amazon Payments EU', 'woocommerce' ); ?></h3>

                <?php $woocommerce->show_messages(); ?>
                <?php do_action('woocommerce_cart_has_errors'); ?>

                <div>

                    <div class="col2-set">
                        <div class="col-1">
                            <div id="AmazonAddressWidget"></div>
                        </div>
                        <div class="col-2">
                            <div id="AmazonWalletWidget"></div>
                        </div>
                    </div>

                    <div id="order_review">

                        <form action='<?php esc_attr_e(WC_Amazon_Checkout::URL); ?>' method='POST' name='amazon-checkout'>
                            <?php $woocommerce->nonce_field('process_checkout')?>
                            <input type='hidden' name='payment_method' value='amazon-checkout'/>
                            <input type='hidden' id='update-order-review_n' name='update-order-review_n' value='<?php esc_attr_e(wp_create_nonce( "update-order-review" )); ?>'/>
                            <input type='hidden' id='update-shipping-method_n' name='update-shipping-method_n' value='<?php esc_attr_e(wp_create_nonce( "update-shipping-method" )); ?>'/>
                            <table class="shop_table">
                                <!--
                                <thead>
                                    <tr>
                                        <th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
                                        <th class="product-total"><?php _e( 'Total', 'woocommerce' ); ?></th>
                                    </tr>
                                </thead>
                                -->
                                <tbody>
                                        <tr class="cart">
                                            <th><?php _e( 'Items', 'woocommerce' ); ?></th>
                                            <td><?php woocommerce_get_template( 'cart/mini-cart.php' ); ?></td>
                                        </tr>
                                </tbody>
                                <tfoot>
                                        <tr class="shipping">
                                            <th><?php _e( 'Totals', 'woocommerce' ); ?></th>
                                            <td><div id="wc-ac-shipping"></div></td>
                                        </tr>
                                        <tr class="payment">
                                            <th><?php _e( 'Payment', 'woocommerce' ); ?></th>
                                            <td>
                                                <input type="submit" disabled="disabled" class="button alt" id="place_order" value="<?php _e( 'Pay for order', 'woocommerce' ); ?>" name="amazon_checkout_place_order"/>
                                                    <?php if (woocommerce_get_page_id('terms')>0) : ?>
                                                    <p class="form-row terms">
                                                        <label for="terms" class="checkbox"><?php _e( 'I have read and accept the', 'woocommerce' ); ?> <a href="<?php echo esc_url( get_permalink(woocommerce_get_page_id('terms')) ); ?>" target="_blank"><?php _e( 'terms &amp; conditions', 'woocommerce' ); ?></a></label>
                                                        <input type="checkbox" class="input-checkbox" name="terms" <?php checked( isset( $_POST['terms'] ), true ); ?> id="terms" />
                                                    </p>
                                                    <?php endif; ?>
                                            </td>
                                        </tr>
                                </tfoot>
                            </table>
                        </form>

                    </div> <!-- #order_review -->

                </div><!-- #review -->

            </div> <!-- #amazon_checkout -->

		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_footer(); ?>
